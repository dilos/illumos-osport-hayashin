#
# CDDL HEADER START
#
# The contents of this file are subject to the terms of the
# Common Development and Distribution License (the "License").
# You may not use this file except in compliance with the License.
#
# You can obtain a copy of the license at usr/src/OPENSOLARIS.LICENSE
# or http://www.opensolaris.org/os/licensing.
# See the License for the specific language governing permissions
# and limitations under the License.
#
# When distributing Covered Code, include this CDDL HEADER in each
# file and include the License file at usr/src/OPENSOLARIS.LICENSE.
# If applicable, add the following below this CDDL HEADER, with the
# fields enclosed by brackets "[]" replaced with your own identifying
# information: Portions Copyright [yyyy] [name of copyright owner]
#
# CDDL HEADER END
#

#
# Copyright 2017 Hayashi Naoyuki
#

TARGET_PREFIX=			$(CROSS_TOOLS)/bin/alpha-solaris2.10-
#
alpha_GNU_CC=			$(TARGET_PREFIX)gcc
alpha_GNU_CXX=			$(TARGET_PREFIX)g++
alpha_GNU_CFLAGS=		$(MCPU:%=-mcpu=%) $(MARCH:%=-march=%) -fPIC -O2 -fno-strict-aliasing
alpha_GNU_CCFLAGS=		$(MCPU:%=-mcpu=%) $(MARCH:%=-march=%) -fPIC -O2 -fno-strict-aliasing

GNU_CC=$($(MACH)_GNU_CC)
GNU_CXX=$($(MACH)_GNU_CXX)
GNU_CFLAGS=$($(MACH)_GNU_OPTFLAGS)
GNU_CCFLAGS=$($(MACH)_GNU_OPTFLAGS)

export CW_GCC=$(GNU_CC)
export CW_GCC_DIR=$(CROSS_TOOLS)/bin
export CW_GPLUSPLUS=$(GNU_CXX)
export CW_GPLUSPLUS_DIR=$(CROSS_TOOLS)/bin
#
alpha_CC=			$(CROSS_TOOLS)/bin/cw -_gcc
alpha_CCC=			$(CROSS_TOOLS)/bin/cw -_g++
alpha_CPP=			$(CROSS_TOOLS)/bin/cw -_gcc -_gcc=-E
alpha_AS=			$(CROSS_TOOLS)/bin/cw -_gcc -_gcc=-x -_gcc=assembler-with-cpp
alpha_LD=			$(CROSS_TOOLS)/bin/alpha-solaris2.10-ld
alpha_LINT=			true
alpha_OC=			$(CROSS_TOOLS)/bin/alpha-solaris2.10-objcopy
alpha_NM=			$(CROSS_TOOLS)/bin/alpha-solaris2.10-nm
alpha_AR=			$(CROSS_TOOLS)/bin/alpha-solaris2.10-ar
alpha_RANLIB=			$(CROSS_TOOLS)/bin/alpha-solaris2.10-ranlib
alpha_READELF=			$(CROSS_TOOLS)/bin/alpha-solaris2.10-readelf
alpha_OD=			$(CROSS_TOOLS)/bin/alpha-solaris2.10-objdump

alpha_FORCE_NEEDED_OPT=	--force-add-needed=

alpha_STAND_FLAGS=		-_gcc=-ffreestanding
alpha_STAND_FLAGS+=		-_gcc=-mno-fp-regs
alpha_STAND_FLAGS+=		-_gcc=-fno-jump-tables
alpha_STAND_FLAGS+=		-_gcc=-fno-common
alpha_STAND_FLAGS+=		-_gcc=-fno-PIC
alpha_STAND_FLAGS+=		-_gcc=-fno-builtin
alpha_XARCH=			$(MCPU:%=-_gcc=-mcpu=%) $(MARCH:%=-_gcc=-march=%)
alpha_AS_XARCH=			$(MCPU:%=-_gcc=-mcpu=%) $(MARCH:%=-_gcc=-march=%)
alpha_C_PICFLAGS=		-K PIC
alpha_CC_PICFLAGS=		-K PIC
alpha_C_BIGPICFLAGS=		-K PIC
alpha_CC_BIGPICFLAGS=		-K PIC

alpha_CFLAGS=			$(alpha_XARCH)
alpha_CCFLAGS=			$(alpha_XARCH)
alpha_ASFLAGS=			$(alpha_AS_XARCH)
alpha_COPTFLAG=			-xO3 -_gcc=-fno-omit-frame-pointer -_gcc=-mieee

alpha_ARFLAGS=			crs

AWK=				/usr/bin/awk

CC=				$($(MACH)_CC)
CCC=				$($(MACH)_CCC)
CPP=				$($(MACH)_CPP)
AS=				$($(MACH)_AS)
LD=				$($(MACH)_LD)
LINT=				$($(MACH)_LINT)
OC=				$($(MACH)_OC)
NM=				$($(MACH)_NM)
AR=				$($(MACH)_AR)
RANLIB=				$($(MACH)_RANLIB)
READELF=			$($(MACH)_READELF)
OD=				$($(MACH)_OD)
C_PICFLAGS=			$($(MACH)_C_PICFLAGS)
CC_PICFLAGS=			$($(MACH)_CC_PICFLAGS)
C_BIGPICFLAGS=			$($(MACH)_C_BIGPICFLAGS)
CC_BIGPICFLAGS=			$($(MACH)_CC_BIGPICFLAGS)
FORCE_NEEDED_OPT=		$($(MACH)_FORCE_NEEDED_OPT)

BUILD64=			$(POUND_SIGN)
GCC_ROOT=
GCCLIBDIR=
GCCLIBDIR64=
POST_PROCESS_O=			true
POST_PROCESS_A=			true
POST_PROCESS_SO=		true
POST_PROCESS=			true
PROCESS_COMMENT=		true
STRIP=				true
CTFCONVERT=			true
CTFMERGE=			true
CTFSTABS=			true
CTFSTRIP=			true
DTRACE=				true
ELFDUMP=			true

CCNOAUTOINLINE=
